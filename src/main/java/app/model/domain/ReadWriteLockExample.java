package app.model.domain;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ReadWriteLockExample {

  private static final Logger logger = LogManager.getLogger(ReadWriteLockExample.class);
  private ReadWriteLock readWriteLock = new ReentrantReadWriteLock();
  private List<Integer> resource = new LinkedList<>();

  private Thread writerThread = new Thread(this::writerThreadImpl);
  private Thread processOne = new Thread(this::increaseBy2ThreadImpl);
  private Thread processTwo = new Thread(this::increaseBy3ThreadImpl);

  public ReadWriteLockExample() {
    ExecutorService executor = Executors.newCachedThreadPool();
    executor.submit(writerThread);
    executor.submit(processOne);
    executor.submit(processTwo);
    executor.shutdown();
    try {
      executor.awaitTermination(24L, TimeUnit.HOURS);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void writerThreadImpl() {
    Random random = new Random();
    readWriteLock.writeLock().lock();
    int data;
    try {
      logger.trace(Thread.currentThread().getName() + ": Adding 5 random number to resource");
      for (int i = 0; i < 5; i++) {
        data = random.nextInt(100);
        logger.trace(Thread.currentThread().getName() + ": " + data + " data added to resource");
        resource.add(data);
        Thread.sleep(500);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    } finally {
      readWriteLock.writeLock().unlock();
    }
  }

  private void increaseBy2ThreadImpl() {
    try {
      Thread.sleep(50);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    readWriteLock.readLock().lock();
    try {
      logger.info(Thread.currentThread().getName()
          + ": Increase all data in resource by 2 task started...");
      List<Integer> tmpList = new LinkedList<>();
      for (int i = 0; i < resource.size(); i++) {
        tmpList.add(i, resource.get(i) + 2);
      }
      logger.info(Thread.currentThread().getName() + ": Task finished! Result = " + tmpList);
    } finally {
      readWriteLock.readLock().unlock();
    }
  }

  private void increaseBy3ThreadImpl() {
    try {
      Thread.sleep(50);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    readWriteLock.readLock().lock();
    try {
      logger.info(Thread.currentThread().getName()
          + ": Increase all data in resource by 3 task started...");
      List<Integer> tmpList = new LinkedList<>();
      for (int i = 0; i < resource.size(); i++) {
        tmpList.add(i, resource.get(i) + 3);
      }
      logger.info(Thread.currentThread().getName() + ": Task finished! Result = " + tmpList);
    } finally {
      readWriteLock.readLock().unlock();
    }
  }
}
