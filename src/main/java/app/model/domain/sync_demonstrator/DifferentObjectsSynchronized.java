package app.model.domain.sync_demonstrator;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DifferentObjectsSynchronized {

  private static final Logger logger = LogManager.getLogger(DifferentObjectsSynchronized.class);
  private Lock lock1 = new ReentrantLock();
  private Lock lock2 = new ReentrantLock();
  private Lock lock3 = new ReentrantLock();

  public void task1() {
      lock1.lock();
      try {
        logger.info(Thread.currentThread().getName() + ": First task is in progress...");
        try {
          logger.info(Thread.currentThread().getName() + ": I am sleeping now");
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        logger.info(Thread.currentThread().getName() + ": First task finished!");
      } finally {
        lock1.unlock();
      }
  }

  public void task2() {
    lock2.lock();
    try {
      logger.info(Thread.currentThread().getName() + ": Second task is in progress...");
      try {
        logger.info(Thread.currentThread().getName() + ": I am sleeping now");
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      logger.info(Thread.currentThread().getName() + ": Second task finished!");
    } finally {
      lock2.unlock();
    }
  }

  public void task3() {
    lock3.lock();
    try {
      logger.info(Thread.currentThread().getName() + ": Third task is in progress...");
      try {
        logger.info(Thread.currentThread().getName() + ": I am sleeping now");
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      logger.info(Thread.currentThread().getName() + ": Third task finished!");
    } finally {
      lock3.unlock();
    }
  }
}
