package app.model.domain;

import java.io.PipedInputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class BlockingQueueCommunication {

  private final static Logger logger = LogManager.getLogger(PipedInputStream.class);
  private BlockingQueue<String> blockingQueue = new LinkedBlockingDeque<>();

  Thread thread1 = new Thread(new Runnable() {
    @Override
    public void run() {
      try {
        blockingQueue.add(Thread.currentThread().getName() + ": I have an apple");
        Thread.sleep(1000);
        blockingQueue.add(Thread.currentThread().getName() + ": I have a pan");
        Thread.sleep(1000);
        blockingQueue.add(Thread.currentThread().getName() + ": Hmmm, Ahhh!");
        Thread.sleep(1000);
        blockingQueue.add(Thread.currentThread().getName() + ": ApplePenPineapplePen");
        Thread.sleep(1000);
        blockingQueue.add("End of Communication!");
      } catch (InterruptedException e) {
        logger.error(e);
      }
    }
  });


  Thread thread2 = new Thread(new Runnable() {
    @Override
    public void run() {
      try {
        String msg;
        while (!(msg = blockingQueue.take()).equals("End of Communication!")) {
          logger.info(Thread.currentThread().getName() + ": Got the msg from other thread: " + msg);
        }
      } catch (InterruptedException e) {
        logger.error(e);
      }
    }
  });

  public BlockingQueueCommunication() throws InterruptedException {
    thread1.start();
    thread2.start();
    thread1.join();
    thread2.join();
  }
}
