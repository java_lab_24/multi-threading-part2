package app.controller;

@FunctionalInterface
public interface SolveAble {

  void solve() throws InterruptedException;
}
