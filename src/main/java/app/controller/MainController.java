package app.controller;

import app.model.domain.BlockingQueueCommunication;
import app.model.domain.ReadWriteLockExample;
import app.model.domain.sync_demonstrator.DifferentObjectsSynchronized;
import app.view.MainView;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MainController {

  private static final Logger logger = LogManager.getLogger(MainController.class);
  private final Scanner scn;
  private MainView view;
  private Map<String, String> menu;
  private Map<String, SolveAble> methodMenu;

  public MainController() {
    view = new MainView();
    scn = new Scanner(System.in);
    menu = new LinkedHashMap<>();
    menu.put("1", "Solve task where methods are synchronized with different Reentrant Locks");
    menu.put("2", "Communication between threads by BlockingQueue");
    menu.put("3", "ReadWriteLock test class");
    menu.put("q", "Quit");
    methodMenu = new HashMap<>();
    methodMenu.put("1", this::pressButton1);
    methodMenu.put("2", this::pressButton2);
    methodMenu.put("3", this::pressButton3);
  }

  private void pressButton1() throws InterruptedException {
    logger.trace("Solving task where methods are synchronized with different Reentrant Locks");
    DifferentObjectsSynchronized differentObjectsSynchronized = new DifferentObjectsSynchronized();
    ExecutorService executorsDifferent = Executors.newFixedThreadPool(3);
    executorsDifferent.submit(differentObjectsSynchronized::task1);
    executorsDifferent.submit(differentObjectsSynchronized::task2);
    executorsDifferent.submit(differentObjectsSynchronized::task3);
    executorsDifferent.shutdown();
    executorsDifferent.awaitTermination(24L, TimeUnit.HOURS);
  }

  private void pressButton2() {
    try {
      new BlockingQueueCommunication();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void pressButton3() {
      new ReadWriteLockExample();
  }

  public void start() {
    String menuInput;
    do {
      System.out.println();
      view.printMenu(menu);
      System.out.print("Please, select menu option: ");
      menuInput = scn.nextLine();
      try {
        methodMenu.get(menuInput).solve();
      } catch (Exception e) {
      }
    } while (!menuInput.equals("q"));
  }
}
